const path = require('path')
const robot = require('robotjs')
const BlinkDiff = require('blink-diff')
const Jimp = require('jimp')
const jf = require('jsonfile')

let items = []

const itemsPath = path.join(__dirname, '..', 'support', 'items.json')
jf.readFile(itemsPath, (err, data) => {
	if (err) console.log(err)
	else items = data
})
	  
module.exports.checkItem = (name) => {
  return getItem(name)
    .then((item) => Promise.all([takeScreenshot(item), Promise.resolve(item.imgRef), Promise.resolve(item.minimumExpected)]))
    .then((result) => {
      let refImg = path.join(__dirname, '..', 'refImages', result[1])
	  let screenshot = path.join(__dirname, '..', '..', 'last.png')
      let expectedSimilarity = result[2]
      return compareImages(screenshot, refImg, expectedSimilarity)
	})
}

const getItem = (name) => {
  return new Promise((res, rej) => {
    let item = items.find((i) => i.name === name)
    if (!item) rej(`${name} not found in database`)
    else res(item)
  })
}

const takeScreenshot = (item) => {
  return new Promise((res, rej) => {
    let position = item ? item.position : null
    if (!position) rej(`${item.name} doesn't have a defined position `)
    let screenshot = robot.screen.capture(position.x, position.y, position.width, position.height)

    // grava imagem capturada para facilitar verificação dos testes falhos
    screenCaptureToFile(screenshot)
      .then((img) => img.write('last.png'))
      .then(() => setTimeout(() => res(screenshot), 500))
  })
}

const compareImages = (screenshot, imgRef, similarity) => {
  return new Promise((res, rej) => {
    let diff = new BlinkDiff({
        imageAPath: screenshot,
        imageBPath: imgRef,
        thresholdType: BlinkDiff.THRESHOLD_PIXEL,
        threshold: 1 - similarity,
        imageOutputPath: 'diff.png'
    })

    diff.run((err, result) => {
      if (err) rej(err)
      else {
        let diffPercentage = result.differences / result.dimension
        if (diffPercentage <= 1 - similarity) res()
        else rej(`${result.differences} differents pixels`)
      }
    })
  })
}

module.exports.clickOnCoordinate = (x, y) => {
  return new Promise((res, rej) => {
	robot.setMouseDelay(0)
	robot.moveMouseSmooth(x, y)
    setTimeout(() => robot.mouseClick(), 500)
	//robot.mouseClick()
	res()
  })
}

const screenCaptureToFile = (robotScreenPic) => {
  return new Promise((res, rej) => {
    try {
      const image = new Jimp(robotScreenPic.width, robotScreenPic.height)
      let pos = 0
      image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
        image.bitmap.data[idx + 2] = robotScreenPic.image.readUInt8(pos++)
        image.bitmap.data[idx + 1] = robotScreenPic.image.readUInt8(pos++)
        image.bitmap.data[idx + 0] = robotScreenPic.image.readUInt8(pos++)
        image.bitmap.data[idx + 3] = robotScreenPic.image.readUInt8(pos++)
      })
      res(image)
    } catch (e) {
      console.error(e)
      rej(e)
    }
  })
}