const { Before } = require('cucumber')
const fs = require('fs')
const path = require('path')

Before(function () {
  [
    'last.png',
    'diff.png'
  ]
  .map(name => path.join(__dirname, '..', '..', name))
  .forEach(file => fs.unlink(file))

})