const { setWorldConstructor } = require('cucumber')
const robot = require('robotjs')

class CustomWorld {
  constructor() {
    this.screen = robot.getScreenSize()
  }
}

setWorldConstructor(CustomWorld)