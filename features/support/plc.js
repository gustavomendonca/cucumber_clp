const snap7 = require('node-snap7')
const plcIP = '192.168.37.131'

let isConnected = false
let s7client = null

const writeMerkerBit = (pos, value) => connectToPLC(plcIP)
  .then(() => readMerkerByte(Math.floor(pos / 8)))
  .then(buf => {
	let byte = buf.readUInt8(0)
    let remainder = pos % 8
    // modifica bit no byte e escreve no plc
    let newByte = (value) ? byte | (1 << remainder) : byte & ~(1 << remainder)
	let v = Buffer.from([newByte])
    return new Promise((res, rej) => s7client.MBWrite(Math.floor(pos / 8), 1, v, (err) => {
      if(err) rej(s7client.ErrorText(err))
      else res()
    }))
  })

const readMerkerByte = (pos) => connectToPLC(plcIP)
  .then(s7client => new Promise((res, rej) => s7client.MBRead(pos, 1, (err, val) => {
      if(err) rej(s7client.ErrorText(err))
      else res(val)
    })
  ))

const connectToPLC = ip => {
  if (isConnected) return s7Client
  else return new Promise((res, rej) => {
    s7client = new snap7.S7Client()
    return s7client.ConnectTo(ip, 0, 1, err => {
      if (err) rej(s7client.ErrorText(err))
      else res(s7client)
    })
  })
}

module.exports = { writeMerkerBit, readMerkerByte }