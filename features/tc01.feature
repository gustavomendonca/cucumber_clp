#encoding: utf-8
#language: pt

Funcionalidade: Comando e Sinalização do TC-01
Como um operador do equipamento
Eu quero comandar e monitorar o transportador de correia
Para continuar os trabalhos na mina

Cenário de Fundo:
  Dado que o TC-01 esteja DESLIGADO
  E que o TC-01 não tenha permissão para ligar
  E que o TC-01 não tenha permissão para funcionar
  Quando aguardo 3 segundos
  
Cenário: Acionamento da permissão para ligar do TC-01
  Então vejo o contator de permissão para ligar do TC-01 DESATIVO
  Quando aciono o comando "PERMISSÃO PARA LIGAR"
  E aguardo 3 segundos
  Então vejo o contator de permissão para ligar do TC-01 ATIVO
  Quando aciono o comando "PERMISSÃO PARA LIGAR"
  E aguardo 3 segundos
  Então vejo o contator de permissão para ligar do TC-01 DESATIVO
  
Cenário: Acionamento da permissão para funcionar do TC-01
  Então vejo o contator de permissão para funcionar do TC-01 DESATIVO
  Quando aciono o comando "PERMISSÃO PARA FUNCIONAR"
  E aguardo 3 segundos
  Então vejo o contator de permissão para funcionar do TC-01 ATIVO
  Quando aciono o comando "PERMISSÃO PARA FUNCIONAR"
  E aguardo 3 segundos
  Então vejo o contator de permissão para funcionar do TC-01 DESATIVO

Cenário: Acionamento do contator do TC-01
  Dado que o TC-01 tenha permissão para ligar
  Dado que o TC-01 tenha permissão para funcionar
  Então vejo o contator do TC-01 DESATIVO
  Quando aciono o comando "LIGAR"
  E aguardo 3 segundos
  Então vejo o contator do TC-01 ATIVO

Cenário: Visualização do estado FUNCIONANDO do TC-01
  Então vejo o indicador de funcionamento do TC-01 DESATIVO
  Quando aciono o comando "LIGAR"
  E aguardo 3 segundos
  Então vejo o indicador de funcionamento do TC-01 ATIVO