const { Given, When, Then } = require('cucumber')
const helper = require('../support/helper')
const plc = require('../support/plc')

const addresses = [
  { name: 'LIGADO', offset: 8 * 8 + 7 },
  { name: 'PERMISSAO_LIGAR', offset: 8 * 8 + 0 },
  { name: 'PERMISSAO_FUNCIONAR', offset: 8 * 8 + 1 }
]

const coordinates = [
  { name: 'LIGAR', x: 150, y: 95 },
  { name: 'PERMISSÃO PARA LIGAR', x: 150, y: 156 },
  { name: 'PERMISSÃO PARA FUNCIONAR', x: 150, y: 216 }
]

Given('que o TC-01 esteja DESLIGADO', function() {
  let addr = addresses.find(v => v.name === 'LIGADO')
  return plc.writeMerkerBit(addr.offset, 0)
})

Given('que o TC-01 tenha permissão para ligar', function () {
  let addr = addresses.find(v => v.name === 'PERMISSAO_LIGAR')
  return plc.writeMerkerBit(addr.offset, 1)
})

Given('que o TC-01 tenha permissão para funcionar', function () {
  let addr = addresses.find(v => v.name === 'PERMISSAO_FUNCIONAR')
  return plc.writeMerkerBit(addr.offset, 1)
})

Given('que o TC-01 não tenha permissão para ligar', function () {
  let addr = addresses.find(v => v.name === 'PERMISSAO_LIGAR')
  return plc.writeMerkerBit(addr.offset, 0)
})

Given('que o TC-01 não tenha permissão para funcionar', function () {
  let addr = addresses.find(v => v.name === 'PERMISSAO_FUNCIONAR')
  return plc.writeMerkerBit(addr.offset, 0)
})

When('aguardo {int} segundos', function (seconds) {
  return new Promise(resolve => setTimeout(resolve, seconds * 1000))
})

When('aciono o comando {string}', { timeout: 10000 }, function (operation) {
  let item = coordinates.find(v => v.name === operation)
  return helper
	.clickOnCoordinate(item.x, item.y)
	//.then(new Promise(resolve => setTimeout(resolve, 2000)))
})

Then('vejo o contator de permissão para ligar do TC-01 ATIVO', function () {
  return helper.checkItem('permissao_ligar_ativo')
})

Then('vejo o contator de permissão para ligar do TC-01 DESATIVO', function () {
  return helper.checkItem('permissao_ligar_desativo')
})

Then('vejo o contator de permissão para funcionar do TC-01 ATIVO', function () {
  return helper.checkItem('permissao_funcionar_ativo')
})

Then('vejo o contator de permissão para funcionar do TC-01 DESATIVO', function () {
  return helper.checkItem('permissao_funcionar_desativo')
})

Then('vejo o contator do TC-01 ATIVO', function () {
  return helper.checkItem('contator_ativo')
})

Then('vejo o contator do TC-01 DESATIVO', function () {
  return helper.checkItem('contator_desativo')
})

Then('vejo o indicador de funcionamento do TC-01 ATIVO', function () {
  return helper.checkItem('contator_funcionando_ativo')
})

Then('vejo o indicador de funcionamento do TC-01 DESATIVO', function () {
  return helper.checkItem('contator_funcionando_desativo')
})
