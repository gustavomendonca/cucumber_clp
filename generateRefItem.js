const robot = require('robotjs')
const path = require('path')
const Jimp = require('jimp')
const jf = require('jsonfile')

if (process.argv.length !== 7) {
  console.log('script deve ser executado na forma: node generateRefImg <nome_arquivo> <x> <y> <width> <height>')
  process.exit()
}

const name = process.argv[2]
const x = process.argv[3]
const y = process.argv[4]
const width = process.argv[5]
const height = process.argv[6]

const screenCaptureToFile = (robotScreenPic) => {
  return new Promise((res, rej) => {
    try {
      const image = new Jimp(robotScreenPic.width, robotScreenPic.height)
      let pos = 0
      image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
        image.bitmap.data[idx + 2] = robotScreenPic.image.readUInt8(pos++)
        image.bitmap.data[idx + 1] = robotScreenPic.image.readUInt8(pos++)
        image.bitmap.data[idx + 0] = robotScreenPic.image.readUInt8(pos++)
        image.bitmap.data[idx + 3] = robotScreenPic.image.readUInt8(pos++)
      })
      res(image)
    } catch (e) {
      console.error(e)
      rej(e)
    }
  })
}

const saveItem = () => {
 let itemsPath = path.join(__dirname, 'features', 'support', 'items.json')
 return openFile(itemsPath)
   .then((items) => { 
     items.push({
     name: name,
     imgRef: `${name}.png`,
     position: {
       x: x,
       y: y,
       width: width,
       height: height
     },
     minimumExpected: 0.98
   })
  return items})
   .then((items) => saveFile(itemsPath, items))
}

const openFile = (fullpath) => {
  return new Promise((res, rej) => {
      jf.readFile(fullpath, (err, items) => {
        if (err) rej(err)
        else res(items)
      })
    })
    .catch(err => {
      if (err.code === 'ENOENT') return []
      else throw err
    })
}

// TODO: garantir que name seja único
const saveFile = (fullpath, data) => {
  return new Promise((res, rej) => {
    jf.writeFile(fullpath, data, {spaces: 2}, (err) => {
      if (err) rej(err)
      else res()
    })
  })
}

const delay = (ms) => {
  return new Promise((res, rej) => {
    setTimeout(() => res(), ms)
  })
}

let fullpath = path.resolve(__dirname, 'features', 'refImages', `${name}.png`)
let robotScreenPic = robot.screen.capture(x, y, width, height)

delay(3000).then(() => screenCaptureToFile(robotScreenPic))
  .then((img) => img.write(fullpath))
  .then(saveItem)
  .then(() => console.log(`${name} criado com sucesso`))
  .catch((e) => console.log(`ERRO: ${e}`))
